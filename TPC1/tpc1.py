from tabulate import tabulate

path = '/home/rito/Desktop/Uni/2º Semestre/PL/pl2023/TPC1/myheart.csv'

def lerGuardarInfo(path):
    
    with open(path, 'r') as f:
        lines = f.readlines()
        
    header = lines[0].strip().split(',')  # separa o cabeçalho
    data = []
    for line in lines[1:]:
        values = line.strip().split(',')  # separa os valores da linha
        row = {}
        for i, header_value in enumerate(header):
            row[header_value] = values[i]
        data.append(row)
    
    return data

dados = lerGuardarInfo(path)

def distDoencaSexo(dados):

    dist_doenca_sexo = {
        'M': {'total': 0, 'temDoenca': 0},
        'F': {'total': 0, 'temDoenca': 0}
    }

    for row in dados:
        sexo = row['sexo']
        temDoenca = row['temDoença']

        dist_doenca_sexo[sexo]['total'] += 1

        if temDoenca == '1':
            dist_doenca_sexo[sexo]['temDoenca'] += 1

    return dist_doenca_sexo

def distDoencaEscEtarios(dados):

    dist_doenca_esc_etarios = {
        '[30-34]': {'total': 0, 'temDoenca': 0},
        '[35-39]': {'total': 0, 'temDoenca': 0},
        '[40-44]': {'total': 0, 'temDoenca': 0},
        '[45-49]': {'total': 0, 'temDoenca': 0},
        '[50-54]': {'total': 0, 'temDoenca': 0},
        '[55-59]': {'total': 0, 'temDoenca': 0},
        '[60-64]': {'total': 0, 'temDoenca': 0},
        '[65-69]': {'total': 0, 'temDoenca': 0},
        '[70-74]': {'total': 0, 'temDoenca': 0},
        '[75-79]': {'total': 0, 'temDoenca': 0},
        '[80-84]': {'total': 0, 'temDoenca': 0},
        '[85-89]': {'total': 0, 'temDoenca': 0},
        '[90-94]': {'total': 0, 'temDoenca': 0},
        '[95-99]': {'total': 0, 'temDoenca': 0}
    }
    
    idade = ['[30-34]', '[35-39]', '[40-44]', '[45-49]', '[50-54]', '[55-59]', '[60-64]', '[65-69]', '[70-74]', '[75-79]', '[80-84]', '[85-89]', '[90-94]', '[95-99]']
    
    for row in dados:
        for i in range(len(idade)):
            if int(row['idade']) in range((i+3)*5-5+1, (i+3)*5+1):
                dist_doenca_esc_etarios[idade[i]]['total'] += 1
                if row['temDoença'] == '1':
                    dist_doenca_esc_etarios[idade[i]]['temDoenca'] += 1
    

    return dist_doenca_esc_etarios  

def distDoencaColesterol(dados):
    dist_doenca_colesterol = {}

    for row in dados:
        colesterol = int(row['colesterol'])
        temDoenca = row['temDoença']
        nivel_colesterol = colesterol // 10 * 10

        if nivel_colesterol not in dist_doenca_colesterol:
            dist_doenca_colesterol[nivel_colesterol] = {'total': 0, 'temDoenca': 0}
        
        dist_doenca_colesterol[nivel_colesterol]['total'] += 1
        
        if temDoenca == '1':
            dist_doenca_colesterol[nivel_colesterol]['temDoenca'] += 1

    sorted_dict = dict(sorted(dist_doenca_colesterol.items(), key=lambda x: x[0]))
    return sorted_dict

def printTableDistribution(distribution):

    if distribution == distDoencaColesterol(dados):
        headers = ['Nível de colesterol', 'Total', 'Tem doença']
    elif distribution == distDoencaSexo(dados):
        headers = ['Total', 'Tem doença']
    elif distribution == distDoencaEscEtarios(dados):
        headers = ['Total', 'Tem doença']
    rows = []

    for level, data in distribution.items():
        rows.append([level, data['total'], data['temDoenca']])

    print(tabulate(rows, headers=headers))

if __name__ == "__main__":
    lerGuardarInfo(path)
    distDoencaSexo(dados)
    distDoencaEscEtarios(dados)
    distDoencaColesterol(dados)
    printTableDistribution(distDoencaSexo(dados))  
    printTableDistribution(distDoencaEscEtarios(dados))
    printTableDistribution(distDoencaColesterol(dados))