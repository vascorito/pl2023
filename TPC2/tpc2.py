import sys

def somaDigitos(text):

    result = 0
    current = ""
    for char in text:
        if char.isdigit():
            current += char
        else:
            if current:
                result += int(current)
                current = ""
    if current:
        result += int(current)
    return result

def main():

    on = False
    total = 0
    for line in sys.stdin:
        line = line.strip().lower()
        if line == "off":
            on = False
        elif line == "on":
            on = True
        elif on:
            for word in line.split():
                if word.isdigit():
                    total += int(word)
        elif "=" in line:
            print(total)
            total = 0


if __name__ == "__main__":
    main()
